<?php
/**
 * Clase de conexiòn
 *
 * Clase para gestionar la conexion a la base de datos
 *
 * @category   Configuracion
 * @package    base de datos
 * @copyright  Copyright (c) 2014-2015 ingenieroweb.com.co
 * @version    $Id:$
 */

class Conexion {

   private $conexion;
   private $consulta;


    /**
     * Contructor que inicia la conexion
     */
    public function __construct (){

       $this->conexion = mysql_connect (DB_HOSTING,DB_USUARIO,DB_CLAVE);
       mysql_select_db(DB);
   }


    /**
     * funcion que ejecuta una consulta sql
     * @param $Sql
     */
    public function query($sql){
       $this->consulta =  mysql_query($sql);
    }


    /**
     * funcion que retorna el resultado de una consulta en objetos
     */
    public function obtenerObjeto(){



        return mysql_fetch_object($this->consulta);
    }


    /**
     * Funcion que finaliza la conexion a la base de datos
     */
    public function desconectar () {
        mysql_close($this->conexion);
    }


} 