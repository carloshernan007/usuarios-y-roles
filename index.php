<?php
/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * LICENSE: Some license information
 *
 * @category   Zend
 * @package    Zend_Magic
 * @subpackage Wand
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */


 /**
  * Incluyo las librerias necesarias
  */
  include('configuracion/db.inc.php');
  include('clases/class.conexion.php');
  include('clases/class.permiso.php');


/**
 * Instanciamos la clase de permisos
 */
$obj_permiso = new Permiso();

/**
 * Le pasamos el id del usuario y el recuros a validar
 */
$permiso = $obj_permiso->validarPermiso(1,1);


/**
 * Validamos si el usuario puede crear una noticia
 */

 if( $permiso->agregar == 1){

     /**
      * Logica para crear las noticias
      */

 }


$obj_permiso->desconectar();

